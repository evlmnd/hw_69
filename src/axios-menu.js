import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://hw-69-somik.firebaseio.com/'
});

// instance.interceptors.request.use(req => {
//     console.log('in request interceptor', req);
//     return req;
// });



export default instance;