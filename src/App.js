import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import Menu from "./components/Menu/Menu";
import Cart from "./components/Cart/Cart";


class App extends Component {
  render() {
    return (
      <div className="App">
        <Menu/>
        <Cart/>
      </div>
    );
  }
}

export default App;
