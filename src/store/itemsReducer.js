const initialState = {
    items: [],
    error: null
};

const itemsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_ITEMS_SUCCESS':
            return {...state, items: action.data};
        case 'GET_ITEMS_ERROR':
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default itemsReducer;