import axios from "../../axios-menu";

export const getItemsRequest = () => ({type: 'GET_ITEMS_REQUEST'});
export const getItemsSuccess = (data) => ({type: 'GET_ITEMS_SUCCESS', data});
export const getItemsFailure = error => ({type: 'GET_ITEMS_ERROR', error});

export const getItems = () => {
    return (dispatch) => {
        dispatch(getItemsRequest());

        axios.get('items.json').then(
            response => {
                dispatch(getItemsSuccess(response.data));
            },
            error => dispatch(getItemsFailure(error))
        );
    }
};