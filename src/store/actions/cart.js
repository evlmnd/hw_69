import axios from '../../axios-menu';

export const addItem = item => ({type: 'ADD_ITEM', item});
export const placeOrder = cart => ({type: 'PLACE_ORDER', cart});
export const removeItem = item => ({type: 'REMOVE_ITEM', item});
export const closeModal = () => ({type: 'CLOSE_MODAL'});

export const orderRequest = () => ({type: 'ORDER_REQUEST'});
export const orderSuccess = () => ({type: 'ORDER_SUCCESS'});
export const orderFailure = () => ({type: 'ORDER_FAILURE'});

export const onPlaceOrder = cart => {
    return dispatch => {
        dispatch(orderRequest());
        axios.post('/orders.json', cart).then(() => {
            dispatch(orderSuccess());
            dispatch(closeModal());
        }, error => {
            dispatch(orderFailure(error));
        })
    }
};