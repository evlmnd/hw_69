const initialState = {
    cart: {},
    orderPlaceable: false,
    loading: false,
    error: null
};

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_ITEM':
            if (state.cart[action.item.name]) {
                return {
                    ...state,
                    isButtonActive: true,
                    cart: {...state.cart, [action.item.name]: {price: action.item.price, count: state.cart[action.item.name].count + 1}}
                }
            } else {
                return {
                    ...state,
                    cart: {...state.cart, [action.item.name]: {price: action.item.price, count: 1}}
                }
            }
        case 'REMOVE_ITEM':
            return {
                ...state,
                cart: {...state.cart, [action.item]: {price: state.cart[action.item].price, count: state.cart[action.item].count - 1}}
            };
        case 'PLACE_ORDER':
            return {
                ...state,
                orderPlaceable: true
            };
        case 'CLOSE_MODAL':
            return {
                ...state,
                orderPlaceable: false
            };
        case 'ORDER_REQUEST':
            return {...state, loading: true};
        case 'ORDER_SUCCESS':
            return {...state, cart: {}, loading: false};
        case 'ORDER_FAILURE':
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default cartReducer;