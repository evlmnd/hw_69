import React from 'react';

const Item = props => {
    return (
        <div className="Item">
            <h5>{props.name}</h5>
            <p>{props.price} KGS</p>
            <button onClick={props.addClick}>Add To Cart</button>
        </div>
    );
};

export default Item;