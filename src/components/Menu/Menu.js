import React, {Component} from 'react';
import {connect} from 'react-redux';
import Item from '../Item/Item';
import {getItems} from "../../store/actions/menu";
import {addItem} from "../../store/actions/cart";


class Menu extends Component {

    componentDidMount() {
        this.props.getItems();
    }

    render() {
        const items = this.props.items.map(item => {
            return <Item
                name={item.name}
                price={item.price}
                key={item.name}
                addClick={() => this.props.addItem(item)}
            />
        });

        return (
            <div className="Menu">
                {items}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        items: state.items.items
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getItems: () => dispatch(getItems()),
        addItem: item => dispatch(addItem(item))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);