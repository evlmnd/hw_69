import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {closeModal, placeOrder, removeItem} from "../../store/actions/cart";
import Modal from "../Modal/Modal";
import ContactData from "../ContactData/ContactData";

class Cart extends Component {

    isDisabled = () => {
        let sum = Object.values(this.props.cart).reduce((sum, el) => sum + el.count, 0);
        return sum === 0;
    };

    getPrice = (item) => {
        return this.props.cart[item].price * this.props.cart[item].count;
    };

    getTotalPrice = () => {
        let sum = Object.values(this.props.cart).reduce((sum, el) => sum + 150 + el.count * el.price, 0);
        return sum;
    };

    render() {
        let items = Object.keys(this.props.cart).map(name => {   /////
            if (this.props.cart[name].count > 0) {
                return <li key={name}>{name} x {this.props.cart[name].count}
                    <span> = {this.getPrice(name)} KGS</span>
                    <button onClick={() => this.props.removeItem(name)}>X</button>
                </li>
            }
        });

        const modal = (this.props.orderPlaceable ? <Modal show={this.props.orderPlaceable} closed={this.props.closeModal}><ContactData/></Modal> : null);

        return (
            <div className="Cart">
                <ul>
                    {items}
                </ul>
                <p>Delivery: 150 KGS</p>
                <p>Total: {this.getTotalPrice()}</p>
                <button
                    onClick={(cart) => this.props.placeOrder(cart)}
                    disabled={this.isDisabled()}
                >Place order
                </button>
                {modal}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        cart: state.cart.cart,
        orderPlaceable: state.cart.orderPlaceable
    };
};

const mapDispatchToProps = dispatch => {
    return {
        placeOrder: cart => dispatch(placeOrder(cart)),
        removeItem: item => dispatch(removeItem(item)),
        closeModal: () => dispatch(closeModal())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);